#!/usr/bin/perl

#use RINGS::Glycan::Convert;

use CGI::Carp qw/fatalsToBrowser/;
use RINGS::Web::HTML;
use RINGS::Glycan::KCF;
use RINGS::Glycan::IUPAC;
use RINGS::Web::user_admin;
use File::Copy;
use RINGS::Glycan::Convert::IupacToKcf;

require 5.001;
require "../../../cgi-lib.pl";


#============
# Get Use ID
#============
my $id = Get_Cookie_Info();

my (%cgi_data,  # The form data
    %cgi_cfn,   # The uploaded file(s) client-provided name(s)
    %cgi_ct,    # The uploaded file(s) content-type(s).  These are
                #   set by the user's browser and may be unreliable
    %cgi_sfn,   # The uploaded file(s) name(s) on the server (this machine)
    $ret,       # Return value of the ReadParse call.       
    $buf,       # Buffer for data read from disk.
);

  $cgi_lib::writefiles = "/tmp";   
 
  $cgi_lib::maxdata = 2000000; 

  $ret = &ReadParse(\%cgi_data,\%cgi_cfn,\%cgi_ct,\%cgi_sfn);
  if (!defined $ret) {
    &CgiDie("Error in reading and parsing of CGI input");
  } elsif (!$ret) {
    &CgiDie("Missing parameters\n",
	    "Please complete the form <a href='fup.html'>fup.html</a>.\n");
  } elsif (!defined $cgi_data{'IUPACfile'} or !defined $cgi_data{'IUPAC'}) {
  	foreach (keys %cgi_data){
		print "CGI = $_ = $cgi_data{$_}<br>";
	}
    &CgiDie("Data missing\n",
	    "Please complete the form <a href='fup.html'>fup.html</a>.\n");
  }

  my $file = "";
  open (UPFILE, $cgi_sfn{'IUPACfile'}) or 
    &CgiError("Error: Unable to open file $cgi_sfn{'IUPACfile'}: $!\n");
  $buf = "";    # avoid annoying warning message
  while (read (UPFILE, $buf, 8192)) {
    $buf =~ s/</&lt;/g;
    $buf =~ s/>/&gt;/g;
    $file .= $buf;
  }
  close (UPFILE);
  unlink ($cgi_sfn{'IUPACfile'}) or
   &CgiError("Error: Unable to delete file",
             "Error: Unable to delete file $cgi_sfn{'IUPACfile'}: $!\n");

  $cgi_lib::writefiles = $cgi_lib::writefiles;
  $cgi_lib::maxdata    = $cgi_lib::maxdata;

  my $inputdata = "";
	if(length $file > 0){
		$file =~ s/\n\n/\n/g;
		$inputdata = $file;
	}else{
		$inputdata = $cgi_data{'IUPAC'};
	}

  my @inputarray = split(/\n/, $inputdata);

  my $time = time();
  $time = int(rand($time));
  if($id !=0)
  {
	mkdir ("/tmp/IUPACtoKCF$time") || die "failed $!";
	my $filename = "/tmp/IUPACtoKCF$time/INPUT.txt";
	open(INPUT,">$filename");
	print INPUT $inputdata;
	close(INPUT);
  }

  if($cgi_data{'type'} eq "html"){ 
	print "Content-type: text/html\n\n"; 
	print "<html>\n";
	print "<head>\n";
	print "<script type=\"text/javascript\">\n";
	print "parent.data_tree.location.href = parent.data_tree.location.href;\n";
	print "</script>\n";
	my $title = "IUPAC to KCF";
	my $head = Make_Head_From_Title2($title);
	$title = "Converted Results";
	my $h1 = Make_H1_From_Header($title);
	print "$head\n";
	print "</head>\n";
	print "<body>\n";
	print "$h1\n";
#	print "If no KCF is displayed (i.e. the original IUPAC is displayed) 
#	       then there may be an error in the IUPAC formatted data.\n";
	print "<pre>";
   } else {
	print "Content-type: text/plain\n\n"; 
   }
   my $iupac = "";

   if($id !=0){
	open OUTPUT, '>', "/tmp/IUPACtoKCF$time/OUTPUT.kcf" or  die "failed $!";
   }



##############
#    main    #
##############

my $kcfs_ref = &IUPACtoKCF(\@inputarray);
my @kcfs = @{$kcfs_ref};
foreach (@kcfs) {
	print $_,"\n";
	print OUTPUT $_,"\n";
}

if($id !=0){
	close(OUTPUT);
}

##############



#===============
# Output
#===============

if($cgi_data{'type'} eq "html"){ 
	print "</pre>";
	print &HtmlBot;
}
my $dbh = Connect_To_Rings();
my $dsname = $cgi_data{'datasetname'};
my $Output_id = "";


#===============
# Data Size
#===============
if($id != 0)
{
	#Get input data size
	$file = "/tmp/IUPACtoKCF$time/INPUT.txt";
	my $Input_data_size = -s $file;

	#Get output data size
	$file = "/tmp/IUPACtoKCF$time/OUTPUT.kcf";
	my $Output_data_size = -s $file;

	$Output_id = Insert_IUPACtoKCF_Data($dbh, $dsname, $Input_data_size, $Output_data_size, $id);
}


#================
# Make Directory
#================
my $SAVE_DIR = "/tmp";

if($id !=0)
{
   $SAVE_DIR = "/var/www/userdata/$id";
   Make_Dir($SAVE_DIR);
   $SAVE_DIR = "$SAVE_DIR/IUPAC to KCF";
   Make_Dir($SAVE_DIR);
   $SAVE_DIR = "$SAVE_DIR/$Output_id";
   Make_Dir($SAVE_DIR);
}


#================
# Move File
#================
if($id !=0)
{
	my $from_dir = "/tmp/IUPACtoKCF$time/";
	my $to_dir = "/var/www/userdata/$id/IUPAC to KCF/$Output_id/";

	move($from_dir."INPUT.txt",$to_dir."INPUT.txt");
	move($from_dir."OUTPUT.kcf",$to_dir."OUTPUT.kcf");
}

=pod
print "<script type=\"text/javascript\">\n";
print "parent.data_tree.location.href = parent.data_tree.location.href;\n";
print "</script>\n";
=cut




################
#  subroutins  # 
################
sub IUPACtoKCF{
   my $inputarray_ref = shift @_;
   my @inputarray = @{$inputarray_ref};

   my @kcfs = ();
   my @disapproves = ();
   my $count2 = 0;
   my $numIN = 0;

   foreach my $iupac (@inputarray) {
      $numIN += 1;
      if($iupac !~ /["\+{]/){
#        $iupac = &Fix_IUPAC_With_Spaces($iupac);
#        my $kcf = &doConvert($iupac);
         my $kcf = IupacToKcf($iupac);		#/usr/lib/perl5/site_perl/RINGS/Glycan/Convert/IupacToKcf.pm
		 if ($kcf =~ /Glycan/) {
            push(@kcfs, $kcf);
		 }else{
            push(@disapproves, $iupac);
            $count2 += 1;
#print "*** not converted *** $kcf<BR>";		 
		 }
      }else{
         push(@disapproves, $iupac);
         $count2 += 1;
#print "*** not converted *** $kcf<BR>";		 
      }
   }

   my $count = 0;
   my $mode = -1;
   my @str_kcf = ();
   $number = 0;

   foreach my $kcf (@kcfs){
      $kcf =~ s/\?//g;

      if($kcf =~ /^ENTRY/){
         $mode = 1;
         $count += 1;
      }elsif($kcf =~ /^NODE/){
         $mode = 2;
      }elsif($kcf =~ /^EDGE/){
         $mode = 3;
      }elsif($kcf =~ /^\/\/\//){
         $mode = 4;
      }elsif($kcf !~ /^\s/ && $mode > 0){
         $mode = -1;
      }

      if($mode > 0){
         push(@str_kcf, $kcf);

      }elsif($mode == -1){
         push(@disapproves, $kcf);
         $count2 += 1;
#print "*** not converted *** $kcf<BR>";		 
      }
   }
   @kcfs = ();
   return (\@str_kcf);
}


sub Fix_IUPAC_With_Spaces
{
   my $line = shift @_;

   chomp($line);
   $line =~ s/\s//g;    #delete space

   if($line =~/^(.+)Sp\d+$/){  #delete the last Sp+number
       $line = $1;
   }
   if($line =~/^(.+)[ab\?][\d-P\/\?]{1,8}$/){ #delete the last number+a/b
       $line = $1;
   }
   if($line =~/^(.+)-$/){       #delete the last -
       $line = $1;
   }

 my $iupac_mode = -1;
  if ($line=~ /\([ab]/){
     $iupac_mode = 1;
  }else{
     $iupac_mode = 2;
  }

   if($iupac_mode == 1){
      $line =~ s/\(/ /g; #add a space bf/af (
      $line =~ s/\)/ /g; #add a space bf/af )
      $line =~ s/\[/ ( /g;   #change [ to (
      $line =~ s/\]/ ) /g;   #change ] to )
   }elsif($iupac_mode == 2){
      $line =~ s/\(/ ( /g;   
      $line =~ s/\)/ ) /g;   
   }

   while ($line =~ /(\d)S/g || $line =~ /S(\d)/g){
      my $templine =  $`." S??-".$1." ".$';
	  $line = $templine;
   }

   while ($line =~ /Me(\d)/g  || $line =~ /(\d)Me/g){
      my $templine =  $`." Me??-".$1." ".$';
	  $line = $templine;
   }

   while ($line =~ /P(\d)/g || $line =~ /(\d)P/g){
      my $templine =  $`." P??-".$1." ".$';
	  $line = $templine;
   }

   $line =~ s/([ab\?][12\?]?-[\d\?]?[\/\?\d]*)/ $1 /g;       #add a space a1/2-number, AKUNE 7/Apr/2015
   $line =~ s/([ab\?][12]-)([^\d\/\?])/ $1 $2/g;    #add a space a1/2-, AKUNE 7/Apr/2015

#   $line =~ s/(a[12]-\d+)/ $1 /g;       #add a space a1/2-number
#   $line =~ s/(b[12]-\d+)/ $1 /g;

#   $line =~ s/(a[12]-)(\D)/ $1 $2/g;    #add a space a1/2-
#   $line =~ s/(b[12]-)(\D)/ $1 $2/g;

#   $line =~ s/(a-\d*)(\D)/ $1 $2/g;     #add a space a-number
#   $line =~ s/(b-\d*)(\D)/ $1 $2/g;

   return $line;
}



sub Make_Dir
{
        my $SAVE_DIR = shift @_;
        if(!-d $SAVE_DIR)
        {
                mkdir ($SAVE_DIR) || die "failed: $!";
        }
}

