#!/usr/bin/perl

use RINGS::Web::HTML;

$id = Get_Cookie_Info();

print "Content-type: text/html\n\n";

print <<EOF;
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ja">
<head>
    <meta http-equiv="Content-Type" content="text/html">

    <meta http-equiv="Content-Style-Type" content="text/css">
    <link rel="stylesheet" href="/rings2.css" type="text/css">

    <title>IUPAC to KCF</title>
<script Language="JavaScript"><!--
function change(obj,col,ft)
{
	obj.style.backgroundColor = col;
	obj.style.color = ft;
}
// --></script>
</head>

<Body><!-- alink="#ff0000" bgcolor="#ffffff" link="#0000ef" text="#000000" vlink="#55188a" -->
<div id =" menu">

<p class="glow"><a href="/index.html"><img src="/rings4.png" border=0></a>
<font size=6>
IUPAC to KCF</font>
</div>
<hr>
<table width=1000 border=0 cellpadding=0 cellspacing=0>
   <tr valign="top">
      <td width = 150>
         <h4 style="text-align : center;" align="center">
         <a href="/index.html" onMouseover="change(this,'#9966FF','#330066')" onMouseout="change(this,'#8484ee','#000033')">Home</a>
         </h4>
         <h4 style="text-align : center;" align="center">
         <a href="/help/help.html" target="_blank" onMouseover="change(this,'#9966FF','#330066')" onMouseout="change(this,'#8484ee','#000033')">Help</a>
         </h4>
        <h4 style="text-align : center;" align="center">
EOF

my $bugTarget = "BugDisplay.pl";
if($id != 0){
   $bugTarget = "BugReportForm.pl?tool=IUPAC to KCF";
}

print <<EOF;
        <a href="/cgi-bin/tools/Bug/$bugTarget" onMouseover="change(this,'#9966FF','#000033')" onMouseout="change(this,'#8484ee','#000033')">Feedback</a>
</h4>
</td>

<td width=750 border=0 cellpadding=0 cellspacing=0>
<Br>
<form action="/cgi-bin/tools/utilities/IUPACtoKCF_au/iupactokcf_au.pl" method="POST" name="IUPACtoKCF" enctype="multipart/form-data">
<b><font size="3">Data set name</font></b>
<input type="text" name="datasetname" value="default">
<font size=1.5>
<Br>
Please enter a glycan structure in IUPAC format for each line.<br>
** Undefined edge information can be written with questin marks.<br>
** Fuzzy edge infromation can be written with "/ (a diagonal)".<br>
** IUPAC structures can be written with square brackets and parentheses, or parentheses. <br>
(Spaces are not considered. Please see below examples.)<br>
<Br>
</font>


<textarea name ="IUPAC" rows="15" cols="80">
GlcNAc(b1-2)Man(a1-6)[Gal(b1-4)GlcNAc(b1-2)[Gal(b1-4)GlcNAc(b1-4)]Man(a1-3)]Man(b1-4)GlcNAc(b1-4)[Fuc(a1-6)]GlcNAc
GlcNAc b1-2 Man a1-6 (Gal b1-4 GlcNAc b1-2 (Gal b1-4 GlcNAc b1-4)Man a1-3)Man b1-4 GlcNAc b1-4(Fuc a1-6)GlcNAc
GlcNAc b1-? Man a1-3/6 (Gal b1-4 GlcNAc b1-2 (Gal b1-4 GlcNAc b1-4)Man a1-3/6)Man b1-4 GlcNAc b1-4(Fuc a1-6)GlcNAc
</textarea>
<br>

Or load it from disk <input name="IUPACfile" type="file">
<P>


<input type="radio" name="type" value="html" checked>HTML
<input type="radio" name="type" value="text">TEXT
<p>
<input value="Clear" onclick="IUPACtoKCF.IUPAC.value='',IUPACtoKCF.datasetname.value=''" type="button">
<input value="Get KCF" type="submit">

</CENTER>

</form>

</body>
</html>
EOF
